<div class="jumbotron card card-image" style="background-image: url(https://mdbootstrap.com/img/Photos/Others/gradient1.jpg);height: 30px;">
  <div class="text-white text-center px-2">
    <div>
      <h3 class="card-title h2-responsive pt-3 mb-5 font-bold"><strong>Carro de Compras</strong></h3>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-8">
    <b>Total: $</b><p id="total">0</p>
  </div>
  <div class="col-md-4">
    <button class="btn btn-info" onclick="hacerPedido()">Hacer Pedido</button>
  </div>
</div>
<hr>
<div class="divScroll">
    <ul class="list-group" id="carrito">
        
    </ul>
</div>


