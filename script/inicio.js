//Funcion de inicio
$(document).ready(function () {
    $.post("sesion.php",{ sesionAdmin: "consultarSesion"})
    .done(function (ippreg){

      if(ippreg.charAt(ippreg.length-1) === "0"){
        location.href = "admin.php";
      }else{
        cargarNavbar();
      }
    });
    
});

//Funcion para cargar el menu
function cargarNavbar() {

    var html =  "<a class='nav-item nav-link active' href='./inicioH.php'>Home<span class='sr-only'>(current)</span></a>"+
                "<a class='nav-item nav-link' href='./contabilidadH.php'>Contabilidad</a>"+
                "<a class='nav-item nav-link' href='./noticiaH.php'>Noticias</a>"+
                "<a class='nav-item nav-link' href='./productoH.php'>Productos</a>"+
                "<a class='nav-item nav-link' href='./pedidoH.php'>Pedidos</a>";
    $('.navbar-nav').html(html);

}