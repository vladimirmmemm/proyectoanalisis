$listaProductos = {};
$cont = 0;

$(document).ready(function () {
    cargarNavbar();
    obtenerProductos();
});
5
//Funcion para cargar el menu
function cargarNavbar() {

    var html =  "<a class='nav-item nav-link' href='./index.php'>Inicio</a>"+
                "<a class='nav-item nav-link ' href='./index.php'>Noticias & Promociones<span class='sr-only'>(current)</span></a>"+
                "<a class='nav-item nav-link active' href='./tiendaH.php'>Tienda</a>";
    $('#menuIndex').html(html);

}
function abrirModalCarro() {
    $('#modelCarro').modal('show');
}

function obtenerProductos(){      
    
    $.ajax({
        url: 'Producto.php',
        type: 'POST',
        data: {obtenerProductos: "true"},
        success: function (resultado) {

            $listaProductos = jQuery.parseJSON(resultado);
            console.log($listaProductos);
            cargarCatalogo();
        }
    });
}

function cargarCatalogo() {
    
    $.each($listaProductos, function( index, value ) {
        $('#catalogo').append(
                                
            '<div class="col-md-4">'+
                '<a class="venobox" data-gall="gallery01" href="'+value.Img_P+'">'+
                    '<img src="'+value.Img_P+'" class="rounded-circle" width="150" height="150">'+
                '</a>'+
                '<h2>'+ value.Nombre_P +'</h2>'+
                '<p>'+ value.Descripcion_P +'</p><hr>'+
                '<p><b>Precio:</b> '+value.Precio_P+'</p>'+
                '<p><a class="btn btn-secondary" onclick="agregarCarrito('+value.Codigo_P+')" role="button">Agregar</a></p>'+
            '</div>'
        );
    });

}

function agregarCarrito(codigo) {
    $.each($listaProductos, function( index, value ) {
        if(value.Codigo_P == codigo){
            $('#carrito').append(
                                
                '<li class="list-group-item d-flex justify-content-between align-items-center" id="li'+value.Codigo_P+'">'+
                    '<span class="badge badge-primary badge-pill"><label id="num'+value.Codigo_P+'">1</label></span>'+
                    value.Nombre_P+
                
                    '<div class="pull-right">'+
                        '<button class="btn btn-light" onclick="disminuir('+value.Codigo_P+')"><i class="fas fa-minus"></i></button>'+
                        '<button class="btn btn-light" onclick="aumentar('+value.Codigo_P+')"><i class="fas fa-plus"></i></button>'+
                        '<button class="btn btn-danger" onclick="eliminar('+value.Codigo_P+')"><i class="far fa-trash-alt"></i></button>'+
                    '</div>'+
                    
                '</li>'
            );
            $total = parseInt($("#total").text());
            $total += parseInt(value.Precio_P);
            $("#total").text($total);
            $cont += 1;
        }
    });
}

function aumentar(id) {
    $num = parseInt($('#num'+id).text());

    $total = parseInt($("#total").text());

    $precio = buscarPrecio(id);

    $total += parseInt($precio);
    $("#total").text($total);
    $('#num'+id).text($num+=1);
}
function disminuir(id) {
    $num = parseInt($('#num'+id).text());

    if($num == 1){
        eliminar(id);
    }else{

        $('#num'+id).text($num-=1);

        $total = parseInt($("#total").text());

        $precio = buscarPrecio(id);

        $total -= parseInt($precio);
        $("#total").text($total);
    }
    
}
function eliminar(id) {
    $num = parseInt($('#num'+id).text());
    $precio = buscarPrecio(id);

    $total -= parseInt($precio*$num);
    $("#total").text($total);

    $cont -= 1;
    $("#li"+id).remove();
}

function buscarPrecio(id) {
    for(var i=0; i<$listaProductos.length; i++){

        if($listaProductos[i].Codigo_P == id){
            return $listaProductos[i].Precio_P;
            i = $listaProductos.length;
        }

    }
}

function hacerPedido() {

    if($cont >= 1){

        $.post("sesion.php",{ sesionUsusaio: "consultarSesion"})
        .done(function (ippreg){

            if(ippreg.charAt(ippreg.length-1) === "0"){
                alertify.error('No hay usuario asignado, inicie sesión');
            }else{

                $cliente = ippreg;
                $imp = 0;
                $total = $('#total').text();

                $.ajax({
                    url: 'contabilidad.php',
                    type: 'POST',
                    dataType: 'html',
                    data: {insertarFV: "true", cliente: $cliente, imp: $imp , 
                        total: $total},
                    success: function (resultado) {
                        if(resultado != 0){
                            insertarDetalleFV(resultado);
                        }else{
                            alertify.error('Ocurrio un error al Agregar la Factura');
                        }
                    }
        
                });
            }
        });

    }else{
       alertify.error('Su factura no tiene productos'); 
    }
}

function insertarDetalleFV(id) {

    $("#carrito li").each(function(index,element){
        $codigo = (element.id).substring(2);
        $cant = $('#num'+$codigo).text();
        alert($cant);

        $.ajax({
            url: 'contabilidad.php',
            type: 'POST',
            dataType: 'html',
            data: {insertarDetalleFV: "true", id: id, producto: $codigo , 
                cant: $cant},
            success: function (resultado) {
                if(resultado == 1){
                    $("#carrito").html("");
                    $cont = 0;
                    $('#total').text("0");
                }else{
                    //alertify.error('Ocurrio un error al Agregar la Factura');
                }
            }

        });
    });

}

