$listaFV = {};
$tablaPadido = null;
//Funcion de inicio
$(document).ready(function () {
    $.post("sesion.php",{ sesionAdmin: "consultarSesion"})
    .done(function (ippreg){

      if(ippreg.charAt(ippreg.length-1) === "0"){
        location.href = "admin.php";
      }else{
        cargarNavbar();
        obtenerPedidos();
      }
    });

});

//Funcion para cargar el menu
function cargarNavbar() {

    var html =  "<a class='nav-item nav-link' href='./inicioH.php'>Home</a>"+
                "<a class='nav-item nav-link' href='./contabilidadH.php'>Contabilidad</a>"+
                "<a class='nav-item nav-link' href='./noticiaH.php'>Noticias</a>"+
                "<a class='nav-item nav-link' href='./productoH.php'>Productos</a>"+ 
                "<a class='nav-item nav-link active' href='./pedidoH.php'>Pedidos<span class='sr-only'>(current)</span></a>";
    $('.navbar-nav').html(html);

}


function obtenerPedidos() {
    $.ajax({
        url: 'Pedido.php',
        type: 'POST',
        data: {obtenerfacturasVentas: "true"},
        success: function (resultado) {

            $listaFV = jQuery.parseJSON(resultado);
            console.log($listaFV);
            inicializarTabla();
        }
    });
}
setInterval(obtenerPedidos, 1000*60);

//Se crea la tabla 
function inicializarTabla() { 

    if($tablaPadido != null){
        $tablaPadido.clear().destroy();
        $tablaPadido = null
    }

    $tablaPadido = $('#tbPedidos').DataTable({
        destroy: true,
        "scrollx": true,
        data: $listaFV,
        columns: [
            { title: "Codigo_FV", data: "Codigo_FV" },
            { title: "Fecha", data: "Fecha"  },
            { title: "Total", data: "Total"},
            { title: "Cliente"},
            { title: "Contenido"},
            { title: "Estado"},
        ],
        "language": {"url": "json/configDatatable.json"},
        "columnDefs": [
        {
            "targets": 3, 
            "data": null,
            "orderable": false,
            "className": "text-center bg-white",
            "mData": function (data, type, val) {
                return "<button id='eliminar' type='button' data-toggle='tooltip' data-placement='top' title='Eliminar selección' class='cont-icono btn btn-outline-danger' disabled><i class='fas fa-user' onclick='buscarCliente("+ data.Codigo_FV +")'></i></button>"
            }
            },{
            "targets": 4, 
            "data": null,
            "orderable": false,
            "className": "text-center bg-white",
            "mData": function (data, type, val) {
                return "<button id='eliminar' type='button' data-toggle='tooltip' data-placement='top' title='Eliminar selección' class='cont-icono btn btn-outline-danger' disabled><i class='fas fa-edit' onclick='mostrarDetalleFV("+ data.Codigo_FV +")'></i></button>"
            }
        },
        {
            "targets": 5, 
            "data": null,
            "orderable": false,
            "className": "text-center bg-white",
            "mData": function (data, type, val) {
                return "<button class='button btn"+data.Estado+"' id='btn"+ data.Codigo_FV +"' onclick='pasarEstado1("+ data.Codigo_FV +")'>"+(parseInt(data.Estado)+1)+"</button>"
            }
        }],
        "order": [[0, "desc"]],
        "autoWidth": false
    });
}

function mostrarDetalleFV(id) {
    $listaTemp = {};
    $('#detalleFV').empty();
    $('#detalleFV').append(
        '<thead>'+
            '<tr>'+
                '<th>Nombre Producto</th>'+
                '<th>Cantidad</th>'+
                '<th>Precio x U</th>'+
            '</tr>'+
        '</thead>'
    );
    $.ajax({
        url: 'contabilidad.php',
        type: 'POST',
        dataType: 'html',
        data: {obtenerDetalleFV: "true", id: id},
        success: function (resultado) {
            $listaTemp = jQuery.parseJSON(resultado);
            

            $.each($listaTemp, function( index, value ) {
              $('#detalleFV').append(
                '<tr>'+
                    '<td>'+value.Nombre_P+'</td>'+
                    '<td>'+value.Cantidad+'</td>'+
                    '<td>'+value.Precio_P+'</td>'+
                '</tr>'
                );
            });
        }

    });
    $('#modalDetalleFV').modal('show');
} 

function buscarCliente(id) {
    $('#detalleFV').empty();
    $('#detalleFV').append(
        '<thead>'+
            '<tr>'+
                '<th>Nombre</th>'+
                '<th>Correo</th>'+
                '<th>Telefono</th>'+
                '<th>Direccion</th>'+
            '</tr>'+
        '</thead>'
    );
    $listaTemp = {};
    $.ajax({
        url: 'Pedido.php',
        type: 'POST',
        dataType: 'html',
        data: {obtenerCliente: "true", id: id},
        success: function (resultado) {
            $listaTemp = jQuery.parseJSON(resultado);
            

            $.each($listaTemp, function( index, value ) {
              $('#detalleFV').append(
                '<tr>'+
                    '<td>'+value.Correo+'</td>'+
                    '<td>'+value.Nombre+'</td>'+
                    '<td>'+value.Telefono+'</td>'+
                    '<td>'+value.Direccion+'</td>'+
                '</tr>'
                );
            });
        }

    });

    $('#modalDetalleFV').modal('show');
}

function pasarEstado1(id) {
    
    $.ajax({
        url: 'Pedido.php',
        type: 'POST',
        data: {actualizarEstado: "true", id: id, estado: 1},
        success: function (resultado) {
            $("#btn"+id).attr('class', 'button btn1');
            $("#btn"+id).text('2');
            $("#btn"+id).attr('onclick', 'pasarEstado2('+id+')');
        }
    });
    
}
function pasarEstado2(id) {
    $.ajax({
        url: 'Pedido.php',
        type: 'POST',
        data: {actualizarEstado: "true", id: id, estado: 2},
        success: function (resultado) {
            obtenerPedidos();
        }
    });
}