$listaNoticias = {};
$tablaNoticias = null;
$Img_N = null;

$(document).ready(function () {
    $.post("sesion.php",{ sesionAdmin: "consultarSesion"})
    .done(function (ippreg){

      if(ippreg.charAt(ippreg.length-1) === "0"){
        location.href = "admin.php";
      }else{
        cargarNavbar();
        obtenerNoticias();
      }
    });

    
});

//Funcion para cargar el menu
function cargarNavbar() {

    var html =  "<a class='nav-item nav-link' href='./inicioH.php'>Inicio</a>"+
                "<a class='nav-item nav-link' href='./contabilidadH.php'>Contabilidad</a>"+
                "<a class='nav-item nav-link active' href='./noticiaH.php'>Noticias<span class='sr-only'>(current)</span></a>"+
                "<a class='nav-item nav-link' href='./productoH.php'>Productos</a>"+
                "<a class='nav-item nav-link' href='./pedidoH.php'>Pedidos</a>";
    $('.navbar-nav').html(html);

}

//Funcion obtener lista noticia
function obtenerNoticias(){      
    $.ajax({
        url: 'Noticia.php',
        type: 'POST',
        data: {obtenerNoticias: "true"},
        success: function (resultado) {

            $listaNoticias = jQuery.parseJSON(resultado);
            inicializarTabla();
            console.log($listaNoticias);     
        }
    });
}

//Se crea la tabla 
function inicializarTabla() { 

    if($tablaNoticias != null){
        $tablaNoticias.clear().destroy();
        $tablaNoticias = null
    }

    $tablaNoticias = $('#tbNoticias').DataTable({
        destroy: true,
        "scrollx": true,
        data: $listaNoticias,
        columns: [
            { title: "Título", data: "Titulo_N" },
            { title: "Contenido"},
            { title: "Fecha", data: "Fecha"  },
            { title: "Imagen"},
            { title: "Editar" },
            { title: "Eliminar" },
        ],
        "language": {"url": "json/configDatatable.json"},
        "columnDefs": [ 
        {
            "targets": 1, 
            "data": null,
            "orderable": false,
            "className": "text-center bg-white",
            "mData": function (data, type, val) {
                return "<button id='eliminar' type='button' data-toggle='tooltip' data-placement='top' title='Eliminar selección' class='cont-icono btn btn-outline-danger' disabled><i class='fas fa-edit' onclick='mostrarContenido("+data.Id_Noticia+")'></i></button>"
            }
        },
        {
            "targets": 3, 
            "data": null,
            "orderable": false,
            "className": "text-center bg-white",
            "mData": function (data, type, val) {
                return "<button id='VizualizarImagen' type='button' data-toggle='tooltip' data-placement='top' title='Vizualizar Imagen' class='cont-icono btn btn-outline-succes' disabled><a class='fas fa-images venobox_custom' data-vbtype='iframe' href='"+data.Img_N+"'></a></button>";
            }
        },
        {
            "targets": 4, 
            "data": null,
            "orderable": false,
            "className": "text-center bg-white",
            "mData": function (data, type, val) {
                 return "<button id='editar' type='button' data-toggle='tooltip' data-placement='top' title='Editar selección' class='cont-icono btn btn-outline-succes' disabled><i class='fas fa-edit' onclick='abrirModalEditar("+data.Id_Noticia+")'></i></button>";
            }
        },
        {
            "targets": 5, 
            "data": null,
            "orderable": false,
            "className": "text-center bg-white",
            "mData": function (data, type, val) {
                 return "<button id='eliminar' type='button' data-toggle='tooltip' data-placement='top' title='Eliminar selección' class='cont-icono btn btn-outline-danger' disabled><i class='far fa-trash-alt' onclick='eliminarNoticia("+data.Id_Noticia+")'></i></button>"
            }
        }
        ],
        "order": [[0, "desc"]],
        "autoWidth": false
    });
}


//Funcion elimiar el noticia
function eliminarNoticia(Id_Noticia){

    swal({
        title: "Esta Seguro de Eliminar la noticia?",
        text: "Al eliminar esta noticia no se puede recuperar!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Sí, Eliminarlo!",
        cancelButtonClass: "btn btn-secondary",
        cancelButtonText: "No, Cancelar!",
        closeOnConfirm: true,
        closeOnCancel: true
    },
    function(isConfirm) {
      if (isConfirm) {
            $.ajax({
                url: 'Noticia.php',
                type: 'POST',
                data: {eliminarNoticia: "true", Id_Noticia: Id_Noticia},
                success: function (resultado) {
                    if(resultado == 1){
                        alertify.success('Su noticia fue eliminado correctamente');
                        obtenerNoticias();
                    }else{
                        alertify.error('Ocurrio un fallo al eliminar la noticia');
                    }          
                }
            });
      } else {
        alertify.error('Cancelo su acción, la noticia esta a salvo');
      }
    });
}

function insertarNoticia(){

    $Titulo_N = $('#Titulo_N').val();
    $Contenido = $('#Contenido').val();
    $Fecha = $('#Fecha').val();

    $.ajax({
        url: 'Noticia.php',
        type: 'POST',
        dataType: 'html',
        data: {insertarNoticia: "true", Titulo_N: $Titulo_N, Contenido: $Contenido , 
            Fecha: $Fecha ,Img_N: $Img_N},
        success: function (resultado) {
            if(resultado == 1){
                alertify.success('Su noticia se agrego correctamente');
                obtenerNoticias();
                $('#modalGeneralNoticia').modal('hide');
            }else{
                alertify.error('Ocurrio un error al Agregar la noticia');
            }
        }

    });

}

function limpiarCampos(){

    $('#Titulo_N').val("");
    $('#Contenido').val("");
    $('#Fecha').val("");
    $('#Img_N').val("");
    $('#Prioridad').val("");

}

function abrirModalRegistrar(){

    limpiarCampos();

    $('#tituloModal').text('Agregar Noticia');
    $('#btnAccion').text('Agregar');
    $('#btnAccion').attr('onclick', 'insertarNoticia()');

    var html =  "<input id='input-b6' name='input-b6' type='file' multiple>";
    $('#vizualizarImagen').html(html);

    $("#input-b6").fileinput({
        theme: 'fas',
        showUpload: false,
        uploadUrl:"http://localhost/procesarImg.php",
        dropZoneEnabled: false,
        maxFileCount: 1,

    }).on('fileuploaded', function(event, previewId, index, fileId) {
        $Img_N = previewId.response.location;
        console.log(previewId.response.location)
    });

    $('#modalGeneralNoticia').modal('show');

}

function mostrarContenido(id) {
    $noticia = "";
    $.each($listaNoticias, function( index, value ) {
        if(value.Id_Noticia == id){
            $noticia = value.Contenido;
        }
    });
    $('#tituloModalConten').text('Contenido Noticia');
    $('#noticiaContenido').text($noticia);

    $('#modalContenido').modal('show');
}

function abrirModalEditar(codigo){

    $('#tituloModal').text('Editar Noticia');
    $('#btnAccion').text('Guardar cambios');
    $('#btnAccion').attr('onclick', 'editarNoticia('+codigo+')');

    limpiarCampos();

    $posicion = buscarNoticia(codigo);


    $('#Titulo_N').val($listaNoticias[$posicion].Titulo_N);
    $('#Contenido').val($listaNoticias[$posicion].Contenido);
    $('#Fecha').val($listaNoticias[$posicion].Fecha);
    $Img_P = $listaNoticias[$posicion].Img_P;

    var html =  "<input id='input-b6' name='input-b6' type='file' multiple>";

    $('#vizualizarImagen').html(html);
    
    $("#input-b6").fileinput({
        theme: 'fas',
        showUpload: false,
        uploadUrl:"http://localhost/procesarImg.php",
        dropZoneEnabled: false,
        maxFileCount: 1,
        mainClass: "input-group-lg",
        initialPreviewAsData: true,
        initialPreview: [
            //Previsualizar la imagen seleccionada
            $Img_P
        ],
        initialPreviewConfig: [
            {caption: $listaNoticias[$posicion].Titulo_N+".jpg", size: 329892, width: "120px", url: "{$url}", key: 1}
        ]
    }).on('fileuploaded', function(event, previewId, index, fileId) {
        $Img_P = previewId.response.location;
        console.log(previewId.response.location)
    });

    $('#modalGeneralNoticia').modal('show');
}

function buscarNoticia(codigo){

    $posicion = 0;

    for(var i = 0; i < $listaNoticias.length; i++){
        if( $listaNoticias[i].Id_Noticia == codigo){
            $posicion = i;
            i = $listaNoticias.length
        }    
    }
    return $posicion;
}

