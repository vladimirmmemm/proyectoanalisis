//Variables Globales
$listaProductos = {};
$tablaProductos = null;
$Img_P = null;

//Funcion de inicio
$(document).ready(function () {
    $.post("sesion.php",{ sesionAdmin: "consultarSesion"})
    .done(function (ippreg){

      if(ippreg.charAt(ippreg.length-1) === "0"){
        location.href = "admin.php";
      }else{
        cargarNavbar();
        obtenerProductos();
      }
    });

});

//Funcion para cargar el menu
function cargarNavbar() {
    
    var html =  "<a class='nav-item nav-link' href='./inicioH.php'>Home</a>"+
                "<a class='nav-item nav-link' href='./contabilidadH.php'>Contabilidad</a>"+
                "<a class='nav-item nav-link' href='./noticiaH.php'>Noticias</a>"+
                "<a class='nav-item nav-link active' href='./productoH.php'>Productos<span class='sr-only'>(current)</span></a>"+
                "<a class='nav-item nav-link' href='./pedidoH.php'>Pedidos</a>";
    $('.navbar-nav').html(html);

}

//Funcion obtener lista productos
function obtenerProductos(){      
    
    $.ajax({
        url: 'Producto.php',
        type: 'POST',
        data: {obtenerProductos: "true"},
        success: function (resultado) {

            $listaProductos = jQuery.parseJSON(resultado);
            inicializarTabla();    
        }
    });
}

//Funcion elimiar el producto
function eliminarProducto(Codigo_P){

    $posicion = buscarProducto(Codigo_P);

    swal({
        title: "Esta Seguro de Eliminar el Producto " + $listaProductos[$posicion].Nombre_P + "?",
        text: "Al eliminar este producto no se puede recuperar!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Sí, Eliminarlo!",
        cancelButtonClass: "btn btn-secondary",
        cancelButtonText: "No, Cancelar!",
        closeOnConfirm: false,
        closeOnCancel: false
    },
    function(isConfirm) {
      if (isConfirm) {
            $.ajax({
                url: 'Producto.php',
                type: 'POST',
                data: {eliminarProducto: "true", Codigo_P: Codigo_P},
                success: function (resultado) {
                    if(resultado == 1){
                       swal("Elimino", "Su producto " + $listaProductos[$posicion].Nombre_P + " fue eliminado correctamente","error");
                        obtenerProductos();
                    }else{
                        swal("Error", "Ocurrio un fallo al eliminar el producto " + $listaProductos[$posicion].Nombre_P , "error");
                    }          
                }
            });
      } else {
        swal("Cancelo", "Su producto " + $listaProductos[$posicion].Nombre_P + " esta a salvo", "error");
      }
    });
}

//Funcion insertar producto
function insertarProducto(){

    $Nombre_P = $('#Nombre_P').val();
    $Descripcion_P = $('#Descripcion_P').val();
    $Precio_P = $('#Precio_P').val();
    $Estado = document.getElementById("customSwitch1").checked;
    console.log($Estado);

    $.ajax({
        url: 'Producto.php',
        type: 'POST',
        dataType: 'html',
        data: {insertarProducto: "true", Nombre_P: $Nombre_P, Descripcion_P: $Descripcion_P , 
            Img_P: $Img_P ,Precio_P: $Precio_P, Estado: $Estado},
        success: function (resultado) {
            if(resultado == 1){
                swal("Agregar", "Su producto " + $Nombre_P + "se agrego correctamente", "success");
                obtenerProductos();
                $('#modalGeneralProducto').modal('hide');
            }else{
                swal("Error", "Ocurrio un error al Agregar Producto " + $Nombre_P, "error");
            }
        }
    });
}

//Funcion editar producto
function editarImgProducto(codigoProducto){

    $Nombre_P = $listaProductos[$posicion].Nombre_P;
    $Descripcion_P = $listaProductos[$posicion].Descripcion_P;
    $Precio_P = $listaProductos[$posicion].Precio_P;
    $Estado = $listaProductos[$posicion].Estado;

    $.ajax({
        url: 'Producto.php',
        type: 'POST',
        dataType: 'html',
        data: {editarProducto: "true", Codigo_P: codigoProducto, Nombre_P: $Nombre_P, Descripcion_P: $Descripcion_P , 
                                       Img_P: $Img_P, Precio_P: $Precio_P, Estado: 0},
        success: function (resultado) {
            console.log(resultado);
            if(resultado == 1){
                swal("Editar", "Su producto " + $Nombre_P + "se edito correctamente", "success");
                obtenerProductos();
                $('#modalGeneralProducto').modal('hide');
            }else{
                swal("Error", "Ocurrio un error al Editar Producto " + $Nombre_P, "error");
            }
        }

    });

}

//Funcion editar producto
function editarProducto(codigoProducto){

    $Nombre_P = $('#Nombre_P').val();
    $Descripcion_P = $('#Descripcion_P').val();
    $Precio_P = $('#Precio_P').val();
    $Estado = $('#Estado').val();

    $.ajax({
        url: 'Producto.php',
        type: 'POST',
        dataType: 'html',
        data: {editarProducto: "true", Codigo_P: codigoProducto, Nombre_P: $Nombre_P, Descripcion_P: $Descripcion_P , 
                                       Img_P: $Img_P, Precio_P: $Precio_P, Estado: 0},
        success: function (resultado) {
            console.log(resultado);
            if(resultado == 1){
                swal("Editar", "Su producto " + $Nombre_P + "se edito correctamente", "success");
                obtenerProductos();
                $('#modalGeneralProducto').modal('hide');
            }else{
                swal("Error", "Ocurrio un error al Editar Producto " + $Nombre_P, "error");
            }
        }

    });

}

//Funcion abrir modal Registrar
function abrirModalRegistrar(){

    $('#tituloModal').text('Registrar Producto');
    $('#btnAccion').text('Agregar');
    $('#btnAccion').attr('onclick', 'insertarProducto()');

    limpiarCampos();

    var html =  "<input id='input-b6' name='input-b6' type='file' multiple>";

    $('#vizualizarImagen').html(html);

    $("#input-b6").fileinput({
        theme: 'fas',
        showUpload: false,
        uploadUrl:"http://localhost/procesarImg.php",
        dropZoneEnabled: false,
        maxFileCount: 1,

    }).on('fileuploaded', function(event, previewId, index, fileId) {
        $Img_P = previewId.response.location;
        console.log(previewId.response.location)
    });

    $('#modalGeneralProducto').modal('show');
}

//Funcion dejar los campos vacios
function limpiarCampos(){

    $('#Nombre_P').val("");
    $('#Descripcion_P').val("");
    $('#Precio_P').val("");
    $('#input-b6').fileinput('clear');

}

//Funcion abrir modal editar
function abrirModalEditar(codigoProducto){

    $('#tituloModal').text('Editar Producto');
    $('#btnAccion').text('Guardar cambios');
    $('#btnAccion').attr('onclick', 'editarProducto('+codigoProducto+')');

    limpiarCampos();

    $posicion = buscarProducto(codigoProducto);

    $('#Nombre_P').val($listaProductos[$posicion].Nombre_P);
    $('#Descripcion_P').val($listaProductos[$posicion].Descripcion_P);
    $('#Precio_P').val($listaProductos[$posicion].Precio_P);
    $Img_P = $listaProductos[$posicion].Img_P;

    var html =  "<input id='input-b6' name='input-b6' type='file' multiple>";


    $('#vizualizarImagen').html(html);
    
    $("#input-b6").fileinput({
        theme: 'fas',
        showUpload: false,
        uploadUrl:"http://localhost/procesarImg.php",
        dropZoneEnabled: false,
        maxFileCount: 1,
        mainClass: "input-group-lg",
        initialPreviewAsData: true,
        initialPreview: [
            //Previsualizar la imagen seleccionada
            $Img_P
        ],
        initialPreviewConfig: [
            {caption: $listaProductos[$posicion].Nombre_P+".jpg", size: 329892, width: "120px", url: "{$url}", key: 1}
        ]
    }).on('fileuploaded', function(event, previewId, index, fileId) {
        $Img_P = previewId.response.location;
        console.log(previewId.response.location)
    });

    $('#modalGeneralProducto').modal('show');
}

//Funcion obtener el objeto producto
function buscarProducto(codigoProducto){

    $posicionProducto = 0;

    for(var i = 0; i < $listaProductos.length; i++){
        if( $listaProductos[i].Codigo_P == codigoProducto){
            $posicionProducto = i;
            i = $listaProductos.length
        }    
    }
    return $posicionProducto;
}

//Se crea la tabla 
function inicializarTabla() { 

    if($tablaProductos != null){
        $tablaProductos.clear().destroy();
        $tablaProductos = null
    }

    $('[data-toggle="tooltip"]').tooltip({ trigger: "hover" });

    $tablaProductos = $('#tbProductos').DataTable({
        destroy: true,
        "scrollx": true,
        data: $listaProductos,
        columns: [
            { title: "Nombre", data: "Nombre_P" },
            { title: "Descripcion", data: "Descripcion_P"  },
            { title: "Precio", data: "Precio_P"  },
            { title: "Editar" },
            { title: "Eliminar" },
        ],
        "language": {"url": "json/configDatatable.json"},
        "columnDefs": [ 
        {
            "targets": 3, 
            "data": null,
            "orderable": false,
            "width": "5%",
            "className": "text-center bg-white",
            "mData": function (data, type, val) {                                                                                                                                                                                     
                 return "<button id='editar' type='button' data-toggle='tooltip' data-placement='top' title='Editar selección' class='cont-icono btn btn-outline-succes' disabled><i class='fas fa-edit' onclick='abrirModalEditar(" + data.Codigo_P + ")'></i></button>";
            }
        },
        {
            "targets": 4, 
            "data": null,
            "orderable": false,
            "width": "5%",
            "className": "text-center bg-white",
            "mData": function (data, type, val) {
                 return "<button id='eliminar' type='button' data-toggle='tooltip' data-placement='top' title='Eliminar selección' class='cont-icono btn btn-outline-danger' disabled><i class='far fa-trash-alt' onclick='eliminarProducto("+data.Codigo_P+")'></i></button>"
            }
        }
        ],
        "order": [[0, "desc"]],
        "autoWidth": false,
        "preDrawCallback": function(settings) {
                $("#tbProductos tbody tr").removeClass("seleccionado");
                $("#tbProductos tbody tr td").removeClass("selected");
            },
        "drawCallback": function(settings) {
            $("#eliminar").prop("disabled", true);
            $("#tbProductos tbody tr").removeClass("selected");
            $('[data-toggle="tooltip"]').tooltip({ trigger: "hover" });
        }
    });
}
