//Funcion de inicio
$(document).ready(function () {
    cargarNavbar();
});

//Funcion para cargar el menu
function cargarNavbar() {

    var html =  "<a class='nav-item nav-link active' href='./index.php'>Inicio<span class='sr-only'>(current)</span></a>"+
                "<a class='nav-item nav-link' href='./noticiasUH.php'>Noticias & Promociones</a>"+
                "<a class='nav-item nav-link' href='./tiendaH.php'>Tienda</a>";
    $('#menuIndex').html(html);
}