//Variables Globales
$listaNoticias = {};

//Funcion de inicio
$(document).ready(function () {
    cargarNavbarCliente();
    obtenerNoticias();
});

//Funcion para cargar el menu de los Clientes
function cargarNavbarCliente() {

    var html =  "<a class='nav-item nav-link' href='./index.php'>Inicio</a>"+
                "<a class='nav-item nav-link active' href='./noticiasUH.php'>Noticias & Promocionesspan <span class='sr-only'>(current)</span></a>"+
                "<a class='nav-item nav-link' href='./tiendaH.php'>Tienda</a>";
    $('#menuIndex').html(html);
}

//Funcion obtener lista noticia
function obtenerNoticias(){      
    $.ajax({
        url: 'Noticia.php',
        type: 'POST',
        data: {obtenerNoticias: "true"},
        success: function (resultado) {
            
            $listaNoticias = jQuery.parseJSON(resultado);
            console.log($listaNoticias);
            distribuirNoticias();   
        }
    });
}

//Funcion llenar noticias
function distribuirNoticias(){
    var html = "";
    $.each($listaNoticias, function( index, noticia ) {
        
        html += "<div class='col-md-4'>"+
                        "<div class='post'>"+
                            "<a class='post-img' href='http://localhost/proyectoanalisis/noticiaUH.php?Id_Noticia="+noticia.Id_Noticia+"'>"+
                                "<img src='"+noticia.Img_N+"' alt=''/>"+
                            "</a>"+
                            "<div class='post-body'>"+
                                "<div class='post-meta'>"+
                                    "<span class='post-date'>"+noticia.Fecha+
                                    "</span>"+
                                "</div>"+
                                "<h3 class='post-title'>"+
                                    "<a href='http://localhost/proyectoanalisis/noticiaUH.php?Id_Noticia="+noticia.Id_Noticia+"'>"+noticia.Titulo_N+"</a>"+
                                "</h3>"+
                                "<p>"+
                                    "<a href='http://localhost/proyectoanalisis/noticiaUH.php?Id_Noticia="+noticia.Id_Noticia+"'>"+noticia.Contenido+"</a>"+
                                "</p>"+

                            "</div>"+
                        "</div>"+
                    "</div>";

        $('#noticia').html(html);
        
    });
}
