$usuario = {};

function insertarUsuario() {
	$correo = $("#rCorreo").val();
	$nombre = $("#rNombre").val();
	$pass = $("#rPass").val();
	$telefono = $("#rTelefono").val();
	$direc = $("#rDirec").val();

	$.ajax({
        url: 'usuario.php',
        type: 'POST',
        dataType: 'html',
        data: {insertarusuario: "true", correo: $correo, nombre: $nombre , 
		telefono: $telefono ,direccion: $direc, pass: $pass},
        success: function (resultado) {
            if(resultado == 1){
				swal("Good job!", "You clicked the button!", "success");
				location.href = "index.php";
            }else{
                swal("Error", "Ocurrio un error al registrarse!!");
            }
        }
    });
}

function consultarUsuario() {
	$correo = $("#inputEmail").val();
	$pass = $("#inputPassword").val();

	$.ajax({
        url: 'usuario.php',
        type: 'POST',
        dataType: 'html',
        data: {obtenerUsuario: "true", correo: $correo, pass: $pass},
        success: function (resultado) {
            $usuario = JSON.parse(resultado);
            if($usuario.length != 0){
                alert($usuario[0].Correo);
                $.post("sesion.php", { sesionUsusaio: "crearSesion",
                idJugador: $usuario[0].Correo}).done(function (data){
                    location.href = "index.php";
                    console.log(data);
                });
                
            }else{
                swal("Error", "Usuario no valida o no existe");
            }
        }
    });
}

function consultarAdmin() {
	$correo = $("#adminEmail").val();
	$pass = $("#adminPass").val();

	$.ajax({
        url: 'usuario.php',
        type: 'POST',
        dataType: 'html',
        data: {obtenerUsuario2: "true", correo: $correo, pass: $pass},
        success: function (resultado) {
            $temp = resultado;
            if($temp != 0){

                $.post("sesion.php", { sesionAdmin: "crearSesion",
                idJugador: $temp}).done(function (data){
                    location.href = "inicioH.php";
                });
                
            }else{
                swal("Error", "Usuario no valida o no existe");
            }
        }
    });
}