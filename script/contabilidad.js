$listaFV = {};
$listaFC = {};
$listaProductos = {};
$tablaFV = null;
$tablaFC = null;

$cont = 0;

//Funcion de inicio
$(document).ready(function () {
    $.post("sesion.php",{ sesionAdmin: "consultarSesion"})
    .done(function (ippreg){

      if(ippreg.charAt(ippreg.length-1) === "0"){
        location.href = "admin.php";
      }else{
        cargarNavbar();
        obtenerFacturas();
      }
    });
    
});

//Funcion para cargar el menu
function cargarNavbar() {

    var html =  "<a class='nav-item nav-link' href='./inicioH.php'>Inicio</a>"+
                "<a class='nav-item nav-link active' href='./contabilidadH.php'>Contabilidad<span class='sr-only'>(current)</span></a>"+
                "<a class='nav-item nav-link' href='./noticiaH.php'>Noticias</a>"+
                "<a class='nav-item nav-link' href='./productoH.php'>Productos</a>"+ 
                "<a class='nav-item nav-link' href='./pedidoH.php'>Pedidos</a>";
                
    $('.navbar-nav').html(html);

}

function obtenerFacturas(){      
    $.ajax({
        url: 'contabilidad.php',
        type: 'POST',
        data: {obtenerfacturasVentas: "true"},
        success: function (resultado) {

            $listaFV = jQuery.parseJSON(resultado);
            inicializarTabla1(); 

            $.ajax({
                url: 'contabilidad.php',
                type: 'POST',
                data: {obtenerfacturasCompras: "true"},
                success: function (resultado) {

                    $listaFC = jQuery.parseJSON(resultado);
                    inicializarTabla2(); 

                    $.ajax({
                        url: 'Producto.php',
                        type: 'POST',
                        data: {obtenerProductos: "true"},
                        success: function (resultado3) {

                            $listaProductos = jQuery.parseJSON(resultado3); 
                        }
                    });
                }
            });
        }
    });
}

//Se crea la tabla 
function inicializarTabla1() { 

    if($tablaFV != null){
        $tablaFV.clear().destroy();
        $tablaFV = null
    }

    $tablaFV = $('#tbfacuraVentas').DataTable({
        destroy: true,
        "scrollx": true,
        data: $listaFV,
        columns: [
            { title: "Codigo_FV", data: "Codigo_FV" },
            { title: "Cliente", data: "Cliente"},
            { title: "Fecha", data: "Fecha"  },
            { title: "Total", data: "Total"},
            { title: "Impuesto", data: "Impuesto"},
            { title: "Contenido"},
            { title: "Eliminar"},
        ],
        "language": {"url": "json/configDatatable.json"},
        "columnDefs": [
        {
            "targets": 5, 
            "data": null,
            "orderable": false,
            "className": "text-center bg-white",
            "mData": function (data, type, val) {
                return "<button id='eliminar' type='button' data-toggle='tooltip' data-placement='top' title='Eliminar selección' class='cont-icono btn btn-outline-danger' disabled><i class='fas fa-edit' onclick='mostrarDetalleFV("+ data.Codigo_FV +")'></i></button>"
            }
        },
        {
            "targets": 6, 
            "data": null,
            "orderable": false,
            "className": "text-center bg-white",
            "mData": function (data, type, val) {
                return "<button id='eliminar' type='button' data-toggle='tooltip' data-placement='top' title='Eliminar selección' class='cont-icono btn btn-outline-danger' disabled><i class='far fa-trash-alt' onclick='eliminarFV("+data.Codigo_FV+")'></i></button>"
            }
        }],
        "order": [[0, "desc"]],
        "autoWidth": false
    });
}

function inicializarTabla2() { 

    if($tablaFC != null){
        $tablaFC.clear().destroy();
        $tablaFC = null
    }

    $tablaFC = $('#tbfacuraCompras').DataTable({
        destroy: true,
        "scrollx": true,
        data: $listaFC,
        columns: [
            { title: "Codigo_FC", data: "Codigo_FC" },
            { title: "Fecha", data: "Fecha"  },
            { title: "Total", data: "Total"},
            { title: "Contenido"},
            { title: "Eliminar"},
        ],
        "language": {"url": "json/configDatatable.json"},
        "columnDefs": [
        {
            "targets": 3, 
            "data": null,
            "orderable": false,
            "className": "text-center bg-white",
            "mData": function (data, type, val) {
                return "<button id='eliminar' type='button' data-toggle='tooltip' data-placement='top' title='Eliminar selección' class='cont-icono btn btn-outline-danger' disabled><i class='fas fa-edit' onclick='mostrarDetalleFC("+ data.Codigo_FC +")'></i></button>"
            }
        },
        {
            "targets": 4, 
            "data": null,
            "orderable": false,
            "className": "text-center bg-white",
            "mData": function (data, type, val) {
                return "<button id='eliminar' type='button' data-toggle='tooltip' data-placement='top' title='Eliminar selección' class='cont-icono btn btn-outline-danger' disabled><i class='far fa-trash-alt' onclick='eliminarFC("+data.Codigo_FC+")'></i></button>"
            }
        }],
        "order": [[0, "desc"]],
        "autoWidth": false
    });
}

function limpiarCamposFV() {
    $('#clienteFV').val('');
    $('#ImpFV').val('');
    $('#totalFV').text('0');
    $('#productoFV').html('');
    $('#tablePFV').html('');
}

function abrirModalRegistrarFV() {
    limpiarCamposFV();
    $('#tituloModalFV').text('Agregar Factura Venta');
    $('#btnAccionFV').text('Insertar');
    $('#btnAccionFV').attr('onclick', 'insertarFV()');

    $.each($listaProductos, function( index, value ) {
      $('#productoFV').append(
        '<option value="'+value.Codigo_P+'">'+value.Nombre_P+'</option>'
        );
    });

    

    $('#modalRegistrarFV').modal('show');
}

function abrirModalRegistrarFC() {
    //limpiarCamposFC();
    $('#tituloModalFC').text('Agregar Factura Compra');
    $('#btnAccionFC').text('Insertar');
    $('#btnAccionFC').attr('onclick', 'insertarFC()');


    

    $('#modalRegistrarFC').modal('show');
}

function insertarFV() {
    if($cont >= 1){
        $cliente = $('#clienteFV').val();
        $imp = $('#ImpFV').val();
        $total = $('#totalFV').text();

        $.ajax({
            url: 'contabilidad.php',
            type: 'POST',
            dataType: 'html',
            data: {insertarFV: "true", cliente: $cliente, imp: $imp , 
                total: $total},
            success: function (resultado) {
                if(resultado != 0){
                    insertarDetalleFV(resultado);
                }else{
                    alertify.error('Ocurrio un error al Agregar la Factura');
                }
            }

        });
    }else{
       alertify.error('Su factura no tiene productos'); 
    }
}

function insertarDetalleFV(id) {
    $('#tablePFV tr').each(function () {

        $codigo = $(this).find("td").eq(0).html();
        $cant = $(this).find("td").eq(2).html();

        $.ajax({
            url: 'contabilidad.php',
            type: 'POST',
            dataType: 'html',
            data: {insertarDetalleFV: "true", id: id, producto: $codigo , 
                cant: $cant},
            success: function (resultado) {
                if(resultado == 1){
                    alertify.success('Su Factura se agrego correctamente');
                    obtenerFacturas();
                    $cont = 0;
                    $('#modalRegistrarFV').modal('hide');
                }else{
                    alertify.error('Ocurrio un error al Agregar la Factura');
                }
            }

        });

    });
}

function insertarFC() {
    if($cont >= 1){
        $total = $('#totalFC').text();

        $.ajax({
            url: 'contabilidad.php',
            type: 'POST',
            dataType: 'html',
            data: {insertarFC: "true", total: $total},
            success: function (resultado) {
                if(resultado != 0){
                    insertarDetalleFC(resultado);
                }else{
                    alertify.error('Ocurrio un error al Agregar la Factura');
                }
            }

        });
    }else{
       alertify.error('Su factura no tiene productos'); 
    }
}

function insertarDetalleFC(id) {
    $('#tablePFC tr').each(function () {

        $nombre = $(this).find("td").eq(0).html();
        $cant = $(this).find("td").eq(1).html();
        $precio = $(this).find("td").eq(2).html();

        $.ajax({
            url: 'contabilidad.php',
            type: 'POST',
            dataType: 'html',
            data: {insertarDetalleFC: "true", id: id, nombre: $nombre , 
                cant: $cant, precio: $precio},
            success: function (resultado) {
                if(resultado == 1){
                    alertify.success('Su Factura se agrego correctamente');
                    obtenerFacturas();
                    $cont = 0;
                    $('#modalRegistrarFC').modal('hide');
                }else{
                    alertify.error('Ocurrio un error al Agregar la Factura');
                }
            }

        });

    });
}

function agregarProductoFV() {
    $index = $('#productoFV').prop('selectedIndex');
    $cant = $('#cantFV').val();

    $('#tablePFV').append(
        '<tr>'+
            '<td>'+$listaProductos[$index].Codigo_P+'</td>'+
            '<td>'+$listaProductos[$index].Nombre_P+'</td>'+
            '<td>'+$cant+'</td>'+
            '<td>'+$listaProductos[$index].Precio_P+'</td>'+
        '</tr>'
    );
    
    $subtotal = (parseInt($cant) * parseInt($listaProductos[$index].Precio_P));
    $('#totalFV').text( parseInt($('#totalFV').text())+ $subtotal);
    $cant = $('#cantFV').val('1');
    $cont += 1;
}

function agregarProductoFC() {
    $nombre = $('#nombrePFC').val();
    $precio = $('#precioFC').val();
    $cant = $('#cantFC').val();

    $('#tablePFC').append(
        '<tr>'+
            '<td>'+$nombre+'</td>'+
            '<td>'+$cant+'</td>'+
            '<td>'+$precio+'</td>'+
        '</tr>'
    );
    
    $subtotal = (parseInt($cant) * parseInt($precio));
    $('#totalFC').text( parseInt($('#totalFC').text())+ $subtotal);

    $('#nombrePFC').val('');
    $('#precioFC').val('');
    $('#cantFC').val('1');
    $cont += 1;
}

function mostrarDetalleFV(id) {
    $listaTemp = {};
    $.ajax({
        url: 'contabilidad.php',
        type: 'POST',
        dataType: 'html',
        data: {obtenerDetalleFV: "true", id: id},
        success: function (resultado) {
            $listaTemp = jQuery.parseJSON(resultado);

            $.each($listaTemp, function( index, value ) {
              $('#detalleFV').append(
                '<tr>'+
                    '<td>'+value.Nombre_P+'</td>'+
                    '<td>'+value.Cantidad+'</td>'+
                    '<td>'+value.Precio_P+'</td>'+
                '</tr>'
                );
            });
        }

    });
    $('#modalDetalleFV').modal('show');
}

function mostrarDetalleFC(id) {
    $listaTemp = {};
    $.ajax({
        url: 'contabilidad.php',
        type: 'POST',
        dataType: 'html',
        data: {obtenerDetalleFC: "true", id: id},
        success: function (resultado) {
            $listaTemp = jQuery.parseJSON(resultado);

            $.each($listaTemp, function( index, value ) {
              $('#detalleFC').append(
                '<tr>'+
                    '<td>'+value.Nombre_S+'</td>'+
                    '<td>'+value.Cantidad_S+'</td>'+
                    '<td>'+value.Precio_S+'</td>'+
                '</tr>'
                );
            });
        }

    });
    $('#modalDetalleFC').modal('show');
}

function eliminarFV(id) {
    swal({
        title: "Esta Seguro de Eliminar la Factura?",
        text: "Al eliminar esta Factura no se puede recuperar!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Sí, Eliminarlo!",
        cancelButtonClass: "btn btn-secondary",
        cancelButtonText: "No, Cancelar!",
        closeOnConfirm: true,
        closeOnCancel: true
    },
    function(isConfirm) {
      if (isConfirm) {
            $.ajax({
                url: 'contabilidad.php',
                type: 'POST',
                data: {eliminarFV: "true", id: id},
                success: function (resultado) {
                    if(resultado == 1){
                        alertify.success('La factura fue eliminado correctamente');
                        obtenerFacturas();
                    }else{
                        alertify.error('Ocurrio un fallo al eliminar el producto la factura');
                    }          
                }
            });
      } else {
        alertify.error('Su factura esta a salvo');
      }
    });
}

function eliminarFC(id) {
    swal({
        title: "Esta Seguro de Eliminar la Factura?",
        text: "Al eliminar esta Factura no se puede recuperar!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Sí, Eliminarlo!",
        cancelButtonClass: "btn btn-secondary",
        cancelButtonText: "No, Cancelar!",
        closeOnConfirm: true,
        closeOnCancel: true
    },
    function(isConfirm) {
      if (isConfirm) {
            $.ajax({
                url: 'contabilidad.php',
                type: 'POST',
                data: {eliminarFC: "true", id: id},
                success: function (resultado) {
                    if(resultado == 1){
                        alertify.success('La factura fue eliminado correctamente');
                        obtenerFacturas();
                    }else{
                        alertify.error('Ocurrio un fallo al eliminar el producto la factura');
                    }          
                }
            });
      } else {
        alertify.error('Su factura esta a salvo');
      }
    });
}