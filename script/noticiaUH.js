//Variables Globales
$listaNoticias = {};

//Funcion de inicio
$(document).ready(function () {
    cargarNavbarCliente();
    obtenerNoticias();
});

//Funcion para cargar el menu de los Clientes
function cargarNavbarCliente() {

    var html =  "<a class='nav-item nav-link' href='./index.php'>Inicio</a>"+
                "<a class='nav-item nav-link active' href='./noticiasUH.php'>Noticias & Promocionesspan <span class='sr-only'>(current)</span></a>"+
                "<a class='nav-item nav-link' href='./tiendaH.php'>Tienda</a>";
    $('#menuIndex').html(html);
}

//Funcion obtener lista noticia
function obtenerNoticias(){      
    $.ajax({
        url: 'Noticia.php',
        type: 'POST',
        data: {obtenerNoticias: "true"},
        success: function (resultado) {
            
            $listaNoticias = jQuery.parseJSON(resultado);
            console.log($listaNoticias);
            obtenerInformacionNoticia();   
        }
    });
}

/**
 * @param String name
 * @return String
 */
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

//Funcion obtener datos noticia
function obtenerInformacionNoticia(){

    var Id_Noticia = getParameterByName('Id_Noticia');
    console.log("id noticia");
    console.log(Id_Noticia);

    $.each($listaNoticias, function( index, noticia ) {
        if(noticia.Id_Noticia == Id_Noticia){
            console.log("entro");
            var html = "<div id='contenido' class='col-12 col-sm-12 col-md-12 col-lg-8'>"+
                        "<div id='titulo'>"+
                            "<h1 class='noticia_titulo'>"+
                                noticia.Titulo_N+
                            "</h1>"+
                        "</div>"+
                        "<div>"+
                            "<p class='text-muted'>"+
                                "<em> Actualizado el "+
                                    noticia.Fecha+
                                "</em>"+
                            "</p>"+
                        "</div>"+
                        "<div>"+
                            "<img class='mw-100' src='"+noticia.Img_N+"' alt=''/>"+
                        "</div>"+
                        "<div class='cuerpo'>"+
                            noticia.Contenido+
                        "</div>"+
                    "</div>";

            $('.row').html(html);
        }
    });
}
        