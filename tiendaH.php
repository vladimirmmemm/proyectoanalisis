<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tienda</title>

    <?php
      include("./public/head.php");
    ?>
    <link rel="stylesheet" href="css/tienda.css">

    <script src="script/tienda.js"></script>

</head>
<body data-spy="scroll" data-target="#myScrollspy" data-offset="20">

    <?php
      include("./public/menu2.php");
    ?>
    <button class="botonF1" onclick="abrirModalCarro()"><i class='fas fa-cart-arrow-down'></i></button>
    <br>
    <div class="carrito">
        <?php include("carritoH.php");?>
    </div>

    <div class="busqueda">
        <div class="jumbotron card card-image" style="background-image: url(https://mdbootstrap.com/img/Photos/Others/gradient1.jpg);height: 30px;">
            <i class="fa fa-utensils"></i><h2><I>   Joshi's Fast Food Store</i></h2>
        </div>
    </div>

    <div class="catalogo">
        <?php include("catalogoH.php");?>
    </div>


    
<div class="modal" tabindex="-1" role="dialog" id="modelCarro">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php include("carritoH.php");?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary">Save changes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
    
    
    
</body>
</html>