<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ADMIN</title>

    <?php
      include("./public/head.php");
    ?>

    <link rel="stylesheet" href="css/login.css">
    <script src="script/registro.js"></script>

</head>
<body>

<div class="container">
<div class="row">
  <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
    <div class="card card-signin my-5">
      <div class="card-body">
        <h4 class="card-title text-center">Inicio de Sesión</h4>
        <hr>
        <form class="form-signin">
            
            <div class="form-label-group">
            <label for="inputEmail">Correo Electronico</label>
            <input type="email" id="adminEmail" class="form-control" placeholder="Email address" required>
            </div>

            <div class="form-group">
            <label for="inputPassword">Contaseña</label>
            <input type="password" id="adminPass" class="form-control" placeholder="Password" required>
            </div>

            <button class="btn btn-lg btn-primary btn-block text-uppercase" type="button"
            onclick="consultarAdmin()">Iniciar</button>
            <hr class="my-4">
            
        </form>
      </div>
    </div>
  </div>
</div>
</div>
    
</body>
</html>