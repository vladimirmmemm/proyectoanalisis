<?php
require_once 'conexion.php';

session_start();

class Producto {
    
  private $conexion;

    public function __construct() {
    $conexion = new conexion();
        $this->conexion = $conexion->getConexion();
    }
  
    public function obtenerProductos() {
    
        $conexion = $this->conexion;
        $stmtProducto = null;
        $respuesta = "";

        try {

            $sql = "call listaProducto()";

            $stmtProducto = $conexion->prepare( $sql );
            $stmtProducto->execute();

            $resultProducto =$stmtProducto->fetchAll();

            $listaProductos = array();

            foreach ($resultProducto as $row) {

                $producto = (object) [
                    'Codigo_P' => $row['Codigo_P'],
                    'Nombre_P' => $row['Nombre_P'],
                    'Descripcion_P' => $row['Descripcion_P'],
                    'Img_P' => $row['Img_P'],
                    'Precio_P' => $row['Precio_P'],
                    'Estado' => $row['Estado']
                ];

                array_push($listaProductos, $producto);
            }
            
            $stmtProducto->closeCursor();

            return json_encode($listaProductos);

        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function eliminarProducto($Codigo_P) {

        $conexion = $this->conexion;
        $stmt = null;
        try {
            $sql = "call eliminarProducto(:Codigo_P)";
            
            $stmt = $conexion->prepare( $sql );
            $stmt->bindParam(':Codigo_P', $Codigo_P);
            $stmt->execute();
            return 1;

        } catch (Exception $ex) {
            return $ex;
        }

    }

    public function insertarProducto( $Nombre_P, $Descripcion_P, $Img_P, $Precio_P, $Estado ) {
        
        $conexion = $this->conexion;
        $stmt = null;

        try {

            $sql = "call insertarProducto( :Nombre_P, :Descripcion_P, :Img_P, :Precio_P, :Estado )";
            $stmt = $conexion->prepare( $sql );
            $stmt->bindParam(':Nombre_P', $Nombre_P);
            $stmt->bindParam(':Descripcion_P', $Descripcion_P);
            $stmt->bindParam(':Img_P', $Img_P);
            $stmt->bindParam(':Precio_P', $Precio_P);
            $stmt->bindParam(':Estado', $Estado);
            $stmt->execute();

            return 1;

        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function editarProducto( $Codigo_P, $Nombre_P, $Descripcion_P, $Img_P, $Precio_P, $Estado ) {
        
        $conexion = $this->conexion;
        $stmt = null;

        try {
            $sql = "call modificarProducto ( :Nombre_P, :Descripcion_P, :Img_P, :Precio_P,  :Estado, :Codigo  )";
            $stmt = $conexion->prepare( $sql );
            $stmt->bindParam(':Nombre_P', $Nombre_P);
            $stmt->bindParam(':Descripcion_P', $Descripcion_P);
            $stmt->bindParam(':Img_P', $Img_P);
            $stmt->bindParam(':Precio_P', $Precio_P);
            $stmt->bindParam(':Estado', $Estado);
            $stmt->bindParam(':Codigo', $Codigo_P);
            $stmt->execute();

        return 1;

        } catch (Exception $ex) {
            return $ex;
        }
    }
}

$producto = new Producto();

if ( (isset($_POST['obtenerProductos'])) ) {
    echo $producto->obtenerProductos();
}

if ( (isset($_POST['eliminarProducto'])) ) {
    echo $producto->eliminarProducto( $_POST['Codigo_P']);
}

if ( (isset($_POST['insertarProducto'])) ) {
    echo $producto->insertarProducto( $_POST['Nombre_P'] , $_POST['Descripcion_P'],
        $_POST['Img_P'] , $_POST['Precio_P'], $_POST['Estado']);
}

if ( (isset($_POST['editarProducto'])) ) {
    echo $producto->editarProducto( $_POST['Codigo_P'], $_POST['Nombre_P'] , $_POST['Descripcion_P'],
        $_POST['Img_P'] , $_POST['Precio_P'], $_POST['Estado']);
}
