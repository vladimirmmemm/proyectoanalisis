<?php
require_once 'conexion.php';

session_start();

class Noticia {
    
  private $conexion;

    public function __construct() {
    $conexion = new conexion();
        $this->conexion = $conexion->getConexion();
    }
  
    public function obtenerNoticias() {

        $conexion = $this->conexion;
        $stmtNoticia = null;
        $respuesta = "";

        try {

            $sql = "call listaNoticia()";

            $stmtNoticia = $conexion->prepare( $sql );
            $stmtNoticia->execute();

            $resultNoticia =$stmtNoticia->fetchAll();

            $listaNoticias = array();

            foreach ($resultNoticia as $row) {

                $noticia = (object) [
                    'Id_Noticia' => $row['Id_Noticia'],
                    'Titulo_N' => $row['Titulo_N'],
                    'Contenido' => $row['Contenido'],
                    'Fecha' => $row['Fecha'],
                    'Img_N' => $row['Img_N']
                ];

                array_push($listaNoticias, $noticia);
            }
            
            $stmtNoticia->closeCursor();

            return json_encode($listaNoticias);

        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function eliminarNoticia($Id_Noticia) {

        $conexion = $this->conexion;
        $stmt = null;
        try {
            $sql = "call eliminarNoticia(:Id_Noticia)";
            
            $stmt = $conexion->prepare( $sql );
            $stmt->bindParam(':Id_Noticia', $Id_Noticia);
            $stmt->execute();
            return 1;

        } catch (Exception $ex) {
            return $ex;
        }

    }

    public function insertarNoticia( $Titulo_N, $Contenido, $Fecha, $Img_N ) {
        
        $conexion = $this->conexion;
        $stmt = null;

        try {

            $sql = "call insertarNoticia( :Titulo_N, :Contenido, :Fecha, :Img_N )";
            $stmt = $conexion->prepare( $sql );
            $stmt->bindParam(':Titulo_N', $Titulo_N);
            $stmt->bindParam(':Contenido', $Contenido);
            $stmt->bindParam(':Fecha', $Fecha);
            $stmt->bindParam(':Img_N', $Img_N);
            $stmt->execute();

            return 1;

        } catch (Exception $ex) {
            return $ex;
        }
    }
}

$noticia = new Noticia();

if ( (isset($_POST['obtenerNoticias'])) ) {
    echo $noticia->obtenerNoticias();
}

if ( (isset($_POST['eliminarNoticia'])) ) {
    echo $noticia->eliminarNoticia( $_POST['Id_Noticia']);
}

if ( (isset($_POST['insertarNoticia'])) ) {
    echo $noticia->insertarNoticia( $_POST['Titulo_N'] , $_POST['Contenido'],
        $_POST['Fecha'] , $_POST['Img_N']);
}
