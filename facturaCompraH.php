<div class="container">
	
	<a id="agregarFV" class="cont-icono btn btn-outline-primary float-left" data-toggle="tooltip" data-placement="top" title="Crear noticia" onclick="abrirModalRegistrarFC()"><i class="far fa-plus-square" ></i></a>

	<table id="tbfacuraCompras" className="display"></table>

	<div class="modal fade" id="modalRegistrarFC" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <div class="col-sm-11">
              <h3 class="modal-title" id="tituloModalFC"></h3>
            </div>
            <div class="col-sm-1">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="float:right">
              <span aria-hidden="true">&times;</span>
              </button>
            </div>
          </div>
          <div class="modal-body" id="contenidoModalFC">

          	<div class="form-group">
              <label>Producto:</label>
              <input type="text" class="form-control" id="nombrePFC">
            </div>

            <div class="form-group">
              <label>Precio:</label>
              <div class="row">
              	<div class="col-md-6">
              		<input type="text" class="form-control" id="precioFC">
              	</div>
              	<div class="col-md-3">
              		<input type="text" class="form-control" id="cantFC" value="1">
              	</div>
              	<div class="col-md-2">
              		<button type="button" class="btn btn-primary" onclick="agregarProductoFC()">Agregar</button>
              	</div>
              </div>
            </div>

            <div class="form-group">
              <table class="table">
              	<thead>
              		<tr>
              			<th>Nombre Producto</th>
              			<th>Cantidad</th>
              			<th>Precio x U</th>
              		</tr>
              	</thead>
              	<tbody id="tablePFC"></tbody>
              </table>
            </div>

          </div>
          <div class="modal-footer">
          	<label>Total: </label><label id="totalFC"> 0  </label>
          	
            <button type="button" class="btn btn-primary" id="btnAccionFC"></button>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="modalDetalleFC" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <div class="col-sm-11">
              <h3 class="modal-title" id="tituloModalFC"></h3>
            </div>
            <div class="col-sm-1">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="float:right">
              <span aria-hidden="true">&times;</span>
              </button>
            </div>
          </div>
          <div class="modal-body" id="contenidoDetalleFC">
          	<table class="table">
          		<thead>
          			<tr>
          				<th>Nombre Producto</th>
          				<th>Cantidad</th>
              			<th>Precio x U</th>
          			</tr>
          		</thead>
          		<tbody id="detalleFC"></tbody>
          	</table>
          </div>
        </div>
      </div>
    </div>

</div>