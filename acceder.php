<br>
<h4 class="card-title text-center">Inicio de Sesión</h4>
<hr>
<form class="form-signin">
    
    <div class="form-label-group">
    <label for="inputEmail">Correo Electronico</label>
    <input type="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus
    value="<?php if(!empty($_SESSION['email'])){echo $_SESSION['email']; }?>">
    </div>

    <div class="form-group">
    <label for="inputPassword">Contaseña</label>
    <input type="password" id="inputPassword" class="form-control" placeholder="Password" required
    value="<?php if(!empty($_SESSION['pass'])){echo $_SESSION['pass']; } ?>">
    </div>

    <button class="btn btn-lg btn-primary btn-block text-uppercase" type="button"
    onclick="consultarUsuario()">Iniciar</button>
    <hr class="my-4">
    <button class="btn btn-lg btn-primary btn-block text-uppercase" type="button" 
    onclick="window.location = '<?php echo $loginURL ?>';"> Google</button>
    
</form>