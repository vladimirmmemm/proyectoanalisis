<div class="container">

	<a id="agregarFV" class="cont-icono btn btn-outline-primary float-left" data-toggle="tooltip" data-placement="top" title="Crear noticia" onclick="abrirModalRegistrarFV()"><i class="far fa-plus-square" ></i></a>

	<table id="tbfacuraVentas" className="display"></table>

	<div class="modal fade" id="modalRegistrarFV" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <div class="col-sm-11">
              <h3 class="modal-title" id="tituloModalFV"></h3>
            </div>
            <div class="col-sm-1">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="float:right">
              <span aria-hidden="true">&times;</span>
              </button>
            </div>
          </div>
          <div class="modal-body" id="contenidoModalFV">

            <div class="form-group">
              <label >Cliente: </label>
              <input type="text" class="form-control" id="clienteFV">
            </div>
            <div class="form-group">
              <label>Impuesto:</label>
              <input type="text" class="form-control" id="ImpFV">
            </div>

            <div class="form-group">
              <label>Producto:</label>
              <div class="row">
              	<div class="col-md-6">
              		<select id="productoFV" class="form-control"></select>
              	</div>
              	<div class="col-md-3">
              		<input type="text" class="form-control" id="cantFV" value="1">
              	</div>
              	<div class="col-md-2">
              		<button type="button" class="btn btn-primary" onclick="agregarProductoFV()">Agregar</button>
              	</div>
              </div>
            </div>

            <div class="form-group">
              <table class="table">
              	<thead>
              		<tr>
              			<th>Código</th>
              			<th>Nombre Producto</th>
              			<th>Cantidad</th>
              			<th>Precio x U</th>
              		</tr>
              	</thead>
              	<tbody id="tablePFV"></tbody>
              </table>
            </div>

          </div>
          <div class="modal-footer">
          	<label>Total: </label><label id="totalFV"> 0  </label>
          	
            <button type="button" class="btn btn-primary" id="btnAccionFV"></button>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="modalDetalleFV" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <div class="col-sm-11">
              <h3 class="modal-title" id="tituloModalFV"></h3>
            </div>
            <div class="col-sm-1">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="float:right">
              <span aria-hidden="true">&times;</span>
              </button>
            </div>
          </div>
          <div class="modal-body" id="contenidoDetalleFV">
          	<table class="table">
          		<thead>
          			<tr>
          				<th>Nombre Producto</th>
          				<th>Cantidad</th>
              			<th>Precio x U</th>
          			</tr>
          		</thead>
          		<tbody id="detalleFV"></tbody>
          	</table>
          </div>
        </div>
      </div>
    </div>

</div>