<!DOCTYPE html lang="es">
<html lang="es">
  <?php
      include("./public/head.php");
  ?>
  <script src="script/noticia.js"></script>
  <script src="css/producto.css"></script>
  <title>Noticias</title>
  <body>
    <?php
      include("./public/menu.php");
    ?>
   <div class="mb-3">
      <button id="selTodo" type="button" data-toggle="tooltip" data-placement="top" title="Seleccionar todo" class="cont-icono btn btn-outline-info mr-2"><i class="far fa-check-square"></i></button>
      <button id="desTodo" type="button" data-toggle="tooltip" data-placement="top" title="Cancelar selección" class="cont-icono btn btn-outline-info mr-2"><i class="far fa-square"></i></button>
      <button id="eliminar" type="button" data-toggle="tooltip" data-placement="top" title="Eliminar selección" class="cont-icono btn btn-outline-danger" disabled><i class="far fa-trash-alt"></i></button>
      <a id="agregar" class="cont-icono btn btn-outline-primary float-right" data-toggle="tooltip" data-placement="top" title="Crear noticia" onclick="abrirModalRegistrar()"><i class="far fa-plus-square" ></i></a>
    </div>
    <div class="container">
      <div class="mb-3">
        <table id="tbNoticias" className="display"></table>
      </div>
    </div>

    <div class="modal fade" id="modalGeneralNoticia" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <div class="col-sm-11">
              <h3 class="modal-title" id="tituloModal"></h3>
            </div>
            <div class="col-sm-1">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="float:right">
              <span aria-hidden="true">&times;</span>
              </button>
            </div>
          </div>
          <div class="modal-body" id="contenidoModal">
            <div class="form-group">
              <label for="nombre">Título Noticia: </label>
              <input type="text" class="form-control" id="Titulo_N">
            </div>
            <div class="form-group">
              <label for="contenidoN">Contenido Noticia:</label>
              <input type="text" class="form-control" id="Contenido">
            </div>
            <div class="form-group">
              <label for="fechaN">Fecha: </label>
              <input type="date" class="form-control" id="Fecha">
            </div>
            <div id="vizualizarImagen">
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            <button type="button" class="btn btn-primary" id="btnAccion"></button>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="modalContenido" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <div class="col-sm-11">
              <h3 class="modal-title" id="tituloModalConten"></h3>
            </div>
            <div class="col-sm-1">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="float:right">
              <span aria-hidden="true">&times;</span>
              </button>
            </div>
          </div>
          <div class="modal-body" id="noticiaContenido">
            
          </div>
        </div>
      </div>
    </div>
  </body>
</html>



