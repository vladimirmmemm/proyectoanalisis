<?php

  require_once "LoginAPI/config.php";

  $loginURL = $gClient->createAuthUrl();

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="google-signin-client_id" content="324126724463-ufarh6bhdbirbslrsaovch3bha0fg81t.apps.googleusercontent.com">
  
  <title>Document</title>

  <?php
      include("./public/head.php");
  ?>

  <link rel="stylesheet" href="css/login.css">
  <script src="script/registro.js"></script>

</head>
<body>

<div class="container">
<div class="row">
  <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
    <div class="card card-signin my-5">
      <div class="card-body">
      <nav>
        <div class="nav nav-tabs" id="nav-tab" role="tablist">
          <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">LOGIN</a>
          <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Registro</a>
        </div>
      </nav>
      <div class="tab-content" id="nav-tabContent">
        <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
          <?php include("acceder.php");?>
        </div>
        <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
          <?php include("registro.php");?>
        </div>
      </div>
      </div>
    </div>
  </div>
</div>
</div>

</body>
</html>