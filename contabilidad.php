<?php
require_once 'conexion.php';

session_start();

class contabilidad {
    
  private $conexion;

    public function __construct() {
    $conexion = new conexion();
        $this->conexion = $conexion->getConexion();
    }
  
    public function obtenerfacturasVentas() {

        $conexion = $this->conexion;
        $stmtNoticia = null;
        $respuesta = "";

        try {

            $sql = "call listaFacturaVenta()";

            $stmtNoticia = $conexion->prepare( $sql );
            $stmtNoticia->execute();

            $resultNoticia =$stmtNoticia->fetchAll();

            $listaNoticias = array();

            foreach ($resultNoticia as $row) {

                $noticia = (object) [
                    'Codigo_FV' => $row['Codigo_FV'],
                    'Cliente' => $row['Cliente'],
                    'Fecha' => $row['Fecha'],
                    'Total' => $row['Total'],
                    'Impuesto' => $row['Impuesto'],
                    'Estado' => $row['Estado']
                ];

                array_push($listaNoticias, $noticia);
            }
            
            $stmtNoticia->closeCursor();

            return json_encode($listaNoticias);

        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function obtenerDetalleFV($id) {

        $conexion = $this->conexion;
        $stmt = null;
        $respuesta = "";

        try {

            $sql = "call listaDetalleFV(:id)";

            $stmt = $conexion->prepare( $sql );
            $stmt->bindParam(':id', $id);
            $stmt->execute();

            $result =$stmt->fetchAll();

            $lista = array();

            foreach ($result as $row) {

                $detalle = (object) [
                    'Nombre_P' => $row['Nombre_P'],
                    'Cantidad' => $row['Cantidad'],
                    'Precio_P' => $row['Precio_P'],
                ];

                array_push($lista, $detalle);
            }
            
            $stmt->closeCursor();

            return json_encode($lista);

        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function insertarFV( $cliente, $imp, $total ) {
        
        $conexion = $this->conexion;
        $stmt = null;

        try {

            $sql = "call insertarFacturaVenta( :Cliente, :Total, :Impuesto)";
            $stmt = $conexion->prepare( $sql );

            $stmt->bindParam(':Cliente', $cliente);
            $stmt->bindParam(':Total', $total);
            $stmt->bindParam(':Impuesto', $imp);
            $stmt->execute();
            $resultNoticia = $stmt->fetchAll();
            $result = 0;

            foreach ($resultNoticia as $row) {
            	$result = $row['idFV'];
            }

            return $result;

        } catch (Exception $ex) {
            return 0;
        }
    }

    public function insertarDetalleFV( $id, $producto, $cant ) {
        
        $conexion = $this->conexion;
        $stmt = null;

        try {

            $sql = "call insertarDetalleFV( :id, :producto, :cant)";
            $stmt = $conexion->prepare( $sql );

            $stmt->bindParam(':id', $id);
            $stmt->bindParam(':producto', $producto);
            $stmt->bindParam(':cant', $cant);
            $stmt->execute();

            return 1;

        } catch (Exception $ex) {
            return 0;
        }
    }

    public function eliminarFV( $id ) {
        
        $conexion = $this->conexion;
        $stmt = null;

        try {

            $sql = "call eliminarFV( :id)";
            $stmt = $conexion->prepare( $sql );

            $stmt->bindParam(':id', $id);
            $stmt->execute();

            return 1;

        } catch (Exception $ex) {
            return 0;
        }
    }

    //------------------------------------------------------------------------

    public function obtenerfacturasCompras() {

        $conexion = $this->conexion;
        $stmtNoticia = null;
        $respuesta = "";

        try {

            $sql = "call listaFacturaCompra()";

            $stmtNoticia = $conexion->prepare( $sql );
            $stmtNoticia->execute();

            $resultNoticia =$stmtNoticia->fetchAll();

            $listaNoticias = array();

            foreach ($resultNoticia as $row) {

                $noticia = (object) [
                    'Codigo_FC' => $row['Codigo_FC'],
                    'Fecha' => $row['Fecha'],
                    'Total' => $row['Total']
                ];

                array_push($listaNoticias, $noticia);
            }
            
            $stmtNoticia->closeCursor();

            return json_encode($listaNoticias);

        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function obtenerDetalleFC($id) {

        $conexion = $this->conexion;
        $stmt = null;
        $respuesta = "";

        try {

            $sql = "call listaDetalleFC(:id)";

            $stmt = $conexion->prepare( $sql );
            $stmt->bindParam(':id', $id);
            $stmt->execute();

            $result =$stmt->fetchAll();

            $lista = array();

            foreach ($result as $row) {

                $detalle = (object) [
                    'Nombre_S' => $row['Nombre_S'],
                    'Cantidad_S' => $row['Cantidad_S'],
                    'Precio_S' => $row['Precio_S'],
                ];

                array_push($lista, $detalle);
            }
            
            $stmt->closeCursor();

            return json_encode($lista);

        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function insertarFC( $total ) {
        
        $conexion = $this->conexion;
        $stmt = null;

        try {

            $sql = "call insertarFacturaCompra(:total)";
            $stmt = $conexion->prepare( $sql );

            $stmt->bindParam(':total', $total);
            $stmt->execute();
            $resultNoticia = $stmt->fetchAll();
            $result = 0;

            foreach ($resultNoticia as $row) {
            	$result = $row['idFC'];
            }

            return $result;

        } catch (Exception $ex) {
            return 0;
        }
    }

    public function insertarDetalleFC( $id, $nombre, $cant, $precio ) {
        
        $conexion = $this->conexion;
        $stmt = null;

        try {

            $sql = "call insertarDetalleFC( :id, :nombre, :cant, :precio)";
            $stmt = $conexion->prepare( $sql );

            $stmt->bindParam(':id', $id);
            $stmt->bindParam(':nombre', $nombre);
            $stmt->bindParam(':cant', $cant);
            $stmt->bindParam(':precio', $precio);
            $stmt->execute();

            return 1;

        } catch (Exception $ex) {
            return 0;
        }
    }

    public function eliminarFC( $id ) {
        
        $conexion = $this->conexion;
        $stmt = null;

        try {

            $sql = "call eliminarFC( :id)";
            $stmt = $conexion->prepare( $sql );

            $stmt->bindParam(':id', $id);
            $stmt->execute();

            return 1;

        } catch (Exception $ex) {
            return 0;
        }
    }

}

$contabilidad = new contabilidad();

if ( (isset($_POST['obtenerfacturasVentas'])) ) {
    echo $contabilidad->obtenerfacturasVentas();
}
if ( (isset($_POST['obtenerDetalleFV'])) ) {
    echo $contabilidad->obtenerDetalleFV($_POST['id']);
}
if ( (isset($_POST['insertarFV'])) ) {
    echo $contabilidad->insertarFV( $_POST['cliente'] , $_POST['imp'],
        $_POST['total']);
}
if ( (isset($_POST['insertarDetalleFV'])) ) {
    echo $contabilidad->insertarDetalleFV( $_POST['id'] , $_POST['producto'],
        $_POST['cant']);
}
if ( (isset($_POST['eliminarFV'])) ) {
    echo $contabilidad->eliminarFV( $_POST['id'] );
}
//-------------------------------------------------------------------------
if ( (isset($_POST['obtenerfacturasCompras'])) ) {
    echo $contabilidad->obtenerfacturasCompras();
}
if ( (isset($_POST['obtenerDetalleFC'])) ) {
    echo $contabilidad->obtenerDetalleFC($_POST['id']);
}
if ( (isset($_POST['insertarFC'])) ) {
    echo $contabilidad->insertarFC( $_POST['total']);
}
if ( (isset($_POST['insertarDetalleFC'])) ) {
    echo $contabilidad->insertarDetalleFC( $_POST['id'] , $_POST['nombre'],
        $_POST['cant'], $_POST['precio']);
}
if ( (isset($_POST['eliminarFC'])) ) {
    echo $contabilidad->eliminarFC( $_POST['id'] );
}

