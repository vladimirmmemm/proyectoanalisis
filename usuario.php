<?php
require_once 'conexion.php';

session_start();

class usuario {
    
  private $conexion;

    public function __construct() {
    $conexion = new conexion();
        $this->conexion = $conexion->getConexion();
    }

    public function insertarusuario($correo, $nombre, $telefono, $direccion, $pass) {
        
        $conexion = $this->conexion;
        $stmt = null;

        try {

            $sql = "call insertarUsuario( :correo, :nombre, :pass, :direc, :telefono )";
            $stmt = $conexion->prepare( $sql );
            $stmt->bindParam(':correo', $correo);
            $stmt->bindParam(':nombre', $nombre);
            $stmt->bindParam(':pass', $pass);
            $stmt->bindParam(':direc', $direccion);
            $stmt->bindParam(':telefono', $telefono);
            $stmt->execute();

            return 1;

        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function obtenerUsuario($correo, $pass) {

        $conexion = $this->conexion;
        $stmtNoticia = null;
        $respuesta = "";

        try {

            $sql = "call consultarUsuario(:correo, :pass)";

            $stmtNoticia = $conexion->prepare( $sql );
            $stmtNoticia->bindParam(':correo', $correo);
            $stmtNoticia->bindParam(':pass', $pass);
            $stmtNoticia->execute();

            $resultNoticia =$stmtNoticia->fetchAll();

            $listaNoticias = array();

            foreach ($resultNoticia as $row) {

                $noticia = (object) [
                    'Correo' => $row['Correo'],
                    'Nombre_Usuario' => $row['Nombre_Usuario'],
                    'Contrasena' => $row['Contrasena'],
                    'Direccion' => $row['Direccion'],
                    'Telefono' => $row['Telefono']
                ];

                array_push($listaNoticias, $noticia);
            }
            
            $stmtNoticia->closeCursor();

            return json_encode($listaNoticias);

        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function obtenerUsuario2($correo, $pass) {

        $conexion = $this->conexion;
        $stmtNoticia = null;
        $respuesta = "";

        try {

            $sql = "call consultarAdmin(:correo, :pass)";

            $stmtNoticia = $conexion->prepare( $sql );
            $stmtNoticia->bindParam(':correo', $correo);
            $stmtNoticia->bindParam(':pass', $pass);
            $stmtNoticia->execute();

            $resultNoticia =$stmtNoticia->fetchAll();

            $listaNoticias = "";

            foreach ($resultNoticia as $row) {

                $listaNoticias = $row['Correo'];
            }
            
            $stmtNoticia->closeCursor();

            return $listaNoticias;

        } catch (Exception $ex) {
            return 0;
        }
    }

}

$usuario = new usuario();

if ( (isset($_POST['insertarusuario'])) ) {
    echo $usuario->insertarusuario( $_POST['correo'] , $_POST['nombre'],
        $_POST['telefono'] , $_POST['direccion'], $_POST['pass']);
}
if ( (isset($_POST['obtenerUsuario'])) ) {
    echo $usuario->obtenerUsuario( $_POST['correo'] , $_POST['pass']);
}
if ( (isset($_POST['obtenerUsuario2'])) ) {
    echo $usuario->obtenerUsuario2( $_POST['correo'] , $_POST['pass']);
}
