<head >
    <meta charset="UTF-8">
    <link rel="stylesheet" href="lib/bootstrap-4.3.1-dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="lib/sweetalert/sweetalert.css">
    <link rel="stylesheet" type="text/css" href="lib/DataTables-1.10.18/css/jquery.datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/general.css"/>
    <link rel="stylesheet" href="lib/font-awesome/fontawesome-free-5.10.2-web/css/all.css">
    <link rel="stylesheet" href="lib/venobox/venobox.css" type="text/css" media="screen" />

    <link href="lib/fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
    <link href="lib/fileinput/themes/explorer-fas/theme.css" media="all" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="lib/alertifyjs/css/alertify.min.css">

    <script src="lib/jquery-3.2.1-dist/jquery-3.2.1.min.js"></script>
    <script src="lib/bootstrap-4.3.1-dist/js/bootstrap.min.js"></script>
    <script src="lib/bootstrap-4.3.1-dist/js/bootstrap.bundle.min.js"></script>
    <script src="lib/bootstrap-4.3.1-dist/js/bootstrap.bundle.min.js"></script>
    <script src="lib/sweetalert/sweetalert.js"></script>
	<script src="lib/Datatables-1.10.18/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="lib/venobox/venobox.min.js"></script>
    
    <script src="lib/fileinput/js/plugins/piexif.js" type="text/javascript"></script>
    <script src="lib/fileinput/js/plugins/sortable.js" type="text/javascript"></script>
    <script src="lib/fileinput/js/fileinput.js" type="text/javascript"></script>
    <script src="lib/fileinput/js/locales/fr.js" type="text/javascript"></script>
    <script src="lib/fileinput/js/locales/es.js" type="text/javascript"></script>
    <script src="lib/fileinput/themes/fas/theme.js" type="text/javascript"></script>
    <script src="lib/fileinput/themes/explorer-fas/theme.js" type="text/javascript"></script>
    <script src="lib/alertifyjs/alertify.min.js"></script>
	
</head>

