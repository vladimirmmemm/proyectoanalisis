<br>
<h4 class="card-title text-center">Registro</h4>
<hr>
<form class="form-signin">

    <div class="form-label-group">
    <label for="inputEmail">Correo Electronico</label>
    <input type="email" id="rCorreo" class="form-control" placeholder="Email address" required
    value="<?php if(!empty($_SESSION['email'])){echo $_SESSION['email']; } ?>">
    </div>

    <div class="form-group">
    <label for="inputPassword">Nombre:</label>
    <input type="email" id="rNombre" class="form-control" placeholder="Nombre Completo" required
    value="<?php if(!empty($_SESSION['Nombre'])){echo $_SESSION['Nombre']; } ?>">
    </div>

    <div class="form-group">
    <label for="inputPassword">Contaseña</label>
    <input type="password" id="rPass" class="form-control" placeholder="Password" required
    value="<?php if(!empty($_SESSION['pass'])){echo $_SESSION['pass']; } ?>">
    <p>Si su registro uso google <b>NO NECESITA Contaseña</b></p>
    </div>

    <div class="form-label-group">
    <label for="inputEmail">Télefono:</label>
    <input type="email" id="rTelefono" class="form-control" placeholder="Formato 12345678" required>
    </div>

    <div class="form-label-group">
    <label for="inputEmail">Dirección:</label>
    <textarea id="rDirec" cols="10" rows="10"  class="form-control"></textarea required>
    </div>
    <br>
    <button class="btn btn-lg btn-primary btn-block text-uppercase" type="button"
    onclick="insertarUsuario()">Registrar</button>
    <hr class="my-4">
    <button class="btn btn-lg btn-primary btn-block text-uppercase" type="button" 
    onclick="window.location = '<?php echo $loginURL ?>';"> Google</button>
    
</form>