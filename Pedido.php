<?php
require_once 'conexion.php';

session_start();

class Pedido {
    
     private $conexion;

    public function __construct() {
        $conexion = new conexion();
        $this->conexion = $conexion->getConexion();
    }

    public function obtenerfacturasVentas() {

        $conexion = $this->conexion;
        $stmtNoticia = null;
        $respuesta = "";

        try {

            $sql = "call listaPedidos()";

            $stmtNoticia = $conexion->prepare( $sql );
            $stmtNoticia->execute();

            $resultNoticia =$stmtNoticia->fetchAll();

            $listaNoticias = array();

            foreach ($resultNoticia as $row) {

                $noticia = (object) [
                    'Codigo_FV' => $row['Codigo_FV'],
                    'Cliente' => $row['Cliente'],
                    'Fecha' => $row['Fecha'],
                    'Total' => $row['Total'],
                    'Impuesto' => $row['Impuesto'],
                    'Estado' => $row['Estado']
                ];

                array_push($listaNoticias, $noticia);
            }
            
            $stmtNoticia->closeCursor();

            return json_encode($listaNoticias);

        } catch (Exception $ex) {
            return $ex;
        }
    }
  
    public function obtenerCliente($id) {
        $conexion = $this->conexion;
        $stmtNoticia = null;
        $respuesta = "";

        try {

            $sql = "call getCliente(:id)";

            $stmtNoticia = $conexion->prepare( $sql );
            $stmtNoticia->bindParam(':id', $id);
            $stmtNoticia->execute();

            $resultNoticia =$stmtNoticia->fetchAll();

            $listaNoticias = array();

            foreach ($resultNoticia as $row) {

                $noticia = (object) [
                    'Correo' => $row['Correo'],
                    'Nombre' => $row['Nombre_Usuario'],
                    'Telefono' => $row['Telefono'],
                    'Direccion' => $row['Direccion']
                ];

                array_push($listaNoticias, $noticia);
            }
            
            $stmtNoticia->closeCursor();

            return json_encode($listaNoticias);

        } catch (Exception $ex) {
            return $ex;
        }
    }

    public function actualizarEstado( $id, $estado ) {
        
        $conexion = $this->conexion;
        $stmt = null;

        try {

            $sql = "call modificarEstado( :id, :estado)";
            $stmt = $conexion->prepare( $sql );

            $stmt->bindParam(':id', $id);
            $stmt->bindParam(':estado', $estado);
            $stmt->execute();

            return 1;

        } catch (Exception $ex) {
            return 0;
        }
    }
    
}

$Pedido = new Pedido();

if ( (isset($_POST['obtenerfacturasVentas'])) ) {
    echo $Pedido->obtenerfacturasVentas();
}
if ( (isset($_POST['obtenerCliente'])) ) {
    echo $Pedido->obtenerCliente($_POST ["id"]);
}
if ( (isset($_POST['actualizarEstado'])) ) {
    echo $Pedido->actualizarEstado($_POST ["id"],$_POST ["estado"]);
}