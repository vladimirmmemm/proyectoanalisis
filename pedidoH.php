<!DOCTYPE html lang="es">
<html lang="es">
  <?php
      include("./public/head.php");
  ?>
  <link rel="stylesheet" href="css/pedido.css">
  <script src="script/pedido.js"></script>

  <title>Pedidos</title>
  <body>
    <?php
      include("./public/menu.php");
    ?>

    <div class="container">
      <div class="text-center">
        <h1 class="font-weight-light">Joshi's Fast Food</h1>
        <h3 class="lead">Sistema de pedidos</h3>
        <h3 class="lead">1) Cocinando...   2) Enviando...</h3>
      </div>
      <br>
      
      <table id="tbPedidos" className="display"></table>
    </div>


    <div class="modal fade" id="modalDetalleFV" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <div class="col-sm-11">
              <h3 class="modal-title" id="tituloModalFV"></h3>
            </div>
            <div class="col-sm-1">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="float:right">
              <span aria-hidden="true">&times;</span>
              </button>
            </div>
          </div>
          <div class="modal-body" id="contenidoDetalleFV">
          	<table class="table" id="detalleFV">
          		
          	</table>
          </div>
        </div>
      </div>
    </div>

   </body>
</html>



