<!DOCTYPE html lang="es">
<html lang="es">
  <?php
      include("./public/head.php");
  ?>
  <script src="script/contabilidad.js"></script>
  <link rel="stylesheet" type="text/css" href="css/contabilidad.css"> 
  <title>Contabilidad</title>
  <body>
    <?php
      include("./public/menu.php");
    ?>

    <ul class="nav nav-tabs nav-justified md-tabs indigo" id="myTabJust" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="home-tab-just" data-toggle="tab" href="#home-just" role="tab" aria-controls="home-just"
      aria-selected="true">Facturas de Ventas</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="profile-tab-just" data-toggle="tab" href="#profile-just" role="tab" aria-controls="profile-just"
      aria-selected="false">Facturas de Compras</a>
  </li>
</ul>
<div class="tab-content card pt-5" id="myTabContentJust">
  <div class="tab-pane fade show active" id="home-just" role="tabpanel" aria-labelledby="home-tab-just">
    <?php include('facturaVentaH.php') ?>
  </div>
  <div class="tab-pane fade" id="profile-just" role="tabpanel" aria-labelledby="profile-tab-just">
    <?php include('facturaCompraH.php') ?>
  </div>
</div>
   </body>
</html>
